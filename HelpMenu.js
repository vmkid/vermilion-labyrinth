Game.HelpMenu = function (game){



};

Game.HelpMenu.prototype = {

    create:function(game){

        menuText = game.add.text(350,60,'Help', {font: '48px Arial', fill:'#fff', align:'center'}); 
        game.add.text(50,150,'You live in an apocalyptic world, scavenging for supplies. One day,', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(50,200,'you were attacked and woke up in a dungeon with two other people. ', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(50,250,'As you look around the room, you notice supplies on one end of the room,', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(50,300,'no windows and only a door. As you peek outside the door, you hear screams.', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(50,350,'You and your fellow captives must defeat any enemies to aquare the supplies.', {font: '20px Arial', fill:'#fff', align:'center'}); 
        game.add.text(50,400,'Developed by Zachary Koch, Andrew Lang, and Bryan Hauser. ', {font: '20px Arial', fill:'#fff', align:'center'}); 

        this.createButton(game, "Back", 400, 530, 140, 60, 
        function(){
            this.state.start('MainMenu');
        });

    },

    update:function(game){

    },

    createButton:function(game, string, x ,y , w,h, callback){

        var button1 = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);

        button1.anchor.setTo(0.5, 0.5);

        button1.width = w;
        button1.height = h;

        var buttontext = game.add.text(button1.x, button1.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    }

};