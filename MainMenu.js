Game.MainMenu = function (game){



};


Game.MainMenu.prototype = {


    create:function(game){

        menuText = game.add.text(280,60,'Main Menu', {font: '48px Arial', fill:'#fff', align:'center'}); 
        this.stage.backgroundColor = '#7f0000';

        //level select button
        this.createButton(game, "Start Game", 400, 220, 140, 60, 
        function(){
            this.state.start('Level1');
        });


        //controls button
        this.createButton(game, "Controls",400, 310, 140, 60, 
        function(){
            this.state.start('ControlsScreen');
        });

        //help button
        this.createButton(game, "Help", 400, 400, 140, 60, 
        function(){
            this.state.start('HelpMenu');
        });

    },

    update:function(game){


    },

    createButton:function(game, string, x ,y , w,h, callback){

        var button1 = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);

        button1.anchor.setTo(0.5, 0.5);

        button1.width = w;
        button1.height = h;

        var buttontext = game.add.text(button1.x, button1.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    }



};