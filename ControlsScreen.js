Game.ControlsScreen = function (game){



};

Game.ControlsScreen.prototype = {

    create:function(game){

        menuText = game.add.text(310,60,'Controls', {font: '48px Arial', fill:'#fff', align:'center'});
        game.add.text(50,200,'Camera Movement', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(250,200,'↑  ↓	→	←', {font: '20px Arial', fill:'#fff', align:'center'});    
        game.add.text(50,250,'Select Unit', {font: '20px Arial', fill:'#fff', align:'center'});  
        game.add.text(250,250,'Left Mouse Button', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(50,300,'Select Target', {font: '20px Arial', fill:'#fff', align:'center'});  
        game.add.text(250,300,'Right Mouse Button', {font: '20px Arial', fill:'#fff', align:'center'});  

        game.add.text(450,150,'Cheats', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(450,200,'1 - Jump to level 1', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(450,250,'2 - Jump to level 2', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(450,300,'3 - Jump to level 3', {font: '20px Arial', fill:'#fff', align:'center'});
        game.add.text(450,350,'i - Set HP to 1,000,000', {font: '20px Arial', fill:'#fff', align:'center'});

        this.createButton(game, "Back", 400, 530, 140, 60, 
        function(){
            this.state.start('MainMenu');
        });

    },

    update:function(game){

    },

    createButton:function(game, string, x ,y , w,h, callback){

        var button1 = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);

        button1.anchor.setTo(0.5, 0.5);

        button1.width = w;
        button1.height = h;

        var buttontext = game.add.text(button1.x, button1.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    }

};