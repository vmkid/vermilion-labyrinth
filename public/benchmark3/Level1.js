Game.Level1 = function(game){};

var cursors;
var marker;

//var player1;
var enemy1;
var enemy2;
var units;

var phase = 'Decision';
var phasetext;

var ui;
var infoborder;
var overlay;
var pausebutton;

var backgorundlayer;
var blockedlayer;

var pausegroup;
var pausetext;
var resumebutton;
var resumetext;
var exitbutton;
var exittext;
var pausebackground;

var endturn;
var blastbutton;
var dashbutton;
var prepbutton;

//hotkeys
var pKey;

var name = '';
var nametext;
var hp;
var hptext;
var armor;
var armortext;
var move; 
var movetext;
var dash;
var dashtext;

var map;


Game.Level1.prototype = {
    create:function(){
        initalizeLevel(1);        

        map = this.game.add.tilemap('level1');


        cursors = this.game.input.keyboard.createCursorKeys();
 
        //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        map.addTilesetImage('level1tileset', 'level1-tileset');
    
        //create layer
        backgroundlayer = map.createLayer('level1');
        blockedlayer = map.createLayer('blockedlayer');
        game.physics.arcade.enable(blockedlayer);
    
        //collision on blockedLayer
        map.setCollisionBetween(1, 2000, true, 'blockedlayer');
    
        //resizes the game world to match the layer dimensions
        backgroundlayer.resizeWorld();

        units = this.findObjectsByType('spawn', map, 'spawnLocations');
 
        //we know there is just one result
        //player1 = this.game.add.sprite(units[0].x, units[0].y, 'tempsprite');
        characters[1].spriteObject = this.game.add.sprite(units[4].x, units[4].y, characters[1].spriteSheet);
        characters[2].spriteObject = this.game.add.sprite(units[3].x, units[3].y, characters[2].spriteSheet);
        characters[0].spriteObject = this.game.add.sprite(units[2].x, units[2].y, characters[0].spriteSheet);
        enemies[1].spriteObject = this.game.add.sprite(units[1].x, units[1].y, 'tempsprite');
        enemies[0].spriteObject = this.game.add.sprite(units[0].x, units[0].y, 'tempsprite');
        
        game.physics.arcade.enable(characters[0].spriteObject);
        game.physics.arcade.enable(characters[1].spriteObject);
        game.physics.arcade.enable(characters[2].spriteObject);
        game.physics.arcade.enable(enemies[0].spriteObject);
        game.physics.arcade.enable(enemies[1].spriteObject);


        //the camera will follow the player in the world
        this.game.camera.follow(this.player);

        pausegroup = game.add.group();
        
        //move player with cursor keys
        this.cursors = this.game.input.keyboard.createCursorKeys();


        //ui
        ui = game.add.group();

        overlay = ui.create(640,0, 'overlay');
        infoborder = ui.create(648,312, 'charinfo');
        infoborder.fixedToCamera = true;
        overlay.fixedToCamera = true;

        //pausebutton

        //Command buttons
        endturn=this.createButton(game, "End Turn", 724, 574, 146, 40, 
        function(){
          //will trigger action phase sequence
          endturn.input.enabled = false;
          blastbutton.input.enabled=false;
          dashbutton.input.enabled=false;
          prepbutton.input.enabled=false;
          phase = "act";
          act(1);
        });
        endturn.input.enabled=false;

        blastbutton=this.createButton(game, "Blast", 724, 100, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=false;
          dashbutton.input.enabled=true;
          prepbutton.input.enabled=true;

          dashbutton.frame = 1;
          prepbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 3;
                }
          }

        });
        
        dashbutton=this.createButton(game, "Dash", 724, 150, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=false;
          prepbutton.input.enabled=true;

          blastbutton.frame = 1;
          prepbutton.frame = 1;


          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 2;
                }
          }
        });
        prepbutton=this.createButton(game, "Prep", 724, 200, 140, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=true;
          prepbutton.input.enabled=false;

          blastbutton.frame = 1;
          dashbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 1;
                }
          }

        });

        phasetext = game.add.text(655,30,'Decision Phase', {font: '20px Arial', fill:'#fff', align:'center'}); 
        phasetext.fixedToCamera = true;

        //marks clicked tiles
        marker = game.add.graphics();
        marker.lineStyle(2, 0xffffff, 1);
        marker.drawRect(0, 0, 32, 32);


        //set camera over players characters
        game.camera.y = 200;

        //set hotkeys
        this.P = game.input.keyboard.addKey(Phaser.Keyboard.P);

        //disable menu on right clicke
        game.canvas.oncontextmenu = function (e) { e.preventDefault();}

    },

    update:function(){

        //lets bullet collide with wall
        game.physics.arcade.overlap(bullet, blockedlayer, bulletMiss, null, this);
        //lets bullet collide with other characters, will have one for each character
        game.physics.arcade.overlap(bullet, characters[0].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, characters[1].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, characters[2].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[0].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[1].spriteObject, bulletCollision, null ,this);

        //enables or disables end-return
        if(characters[0].action !=0 && characters[1].action !=0 && characters[2].action !=0){
            endturn.input.enabled = true;
        }

        //camera controls
        if(cursors.up.isDown){
          game.camera.y -= 4;
        }
        else if(cursors.down.isDown){
          game.camera.y += 4;
        }


        if(phase == 'Decision'){
            if(game.input.mouse.button==0){
                for(var i=0; i<characters.length; i++){
                    if(Math.floor(characters[i].spriteObject.x/32) == Math.floor((game.input.activePointer.x+game.camera.x)/32) && Math.floor(characters[i].spriteObject.y/32) == Math.floor((game.input.activePointer.y+game.camera.y)/32)){
                        name = characters[i].name;
                        hp = characters[i].health;
                        move = characters[i].speed;
                        armor = characters[i].armor;
                        dash = Math.floor (characters[i].speed/2);
                        if(nametext != null){
                            nametext.destroy()
                        }
                        if(hptext != null){
                            hptext.destroy()
                        }
                        if(armortext != null){
                            armortext.destroy()
                        }
                        if(movetext != null){
                            movetext.destroy()
                        }
                        if(dashtext != null){
                            dashtext.destroy()
                        }
                        nametext = game.add.text(700,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
                        hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
                        armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
                        movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
                        dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

                        nametext.fixedToCamera = true;
                        hptext.fixedToCamera = true;
                        armortext.fixedToCamera = true;
                        movetext.fixedToCamera = true;
                        dashtext.fixedToCamera = true;

                        selectedunit = name;

                        if(characters[i].action == 1){
                            prepbutton.frame = 0;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            prepbutton.input.enabled=false;
                        }
                        else if(characters[i].action == 2){
                            prepbutton.frame = 1;
                            dashbutton.frame = 0;
                            blastbutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=false;
                            prepbutton.input.enabled=true;
                        }
                        else if(characters[i].action == 3){
                            prepbutton.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 0;

                            blastbutton.input.enabled=false;
                            dashbutton.input.enabled=true;
                            prepbutton.input.enabled=true;
                        }
                        else{
                            prepbutton.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            prepbutton.input.enabled=true;
                        }
                    }
                }
                for(var i=0; i<enemies.length; i++){
                    if(Math.floor(enemies[i].spriteObject.x/32) == Math.floor((game.input.activePointer.x+game.camera.x)/32) && Math.floor(enemies[i].spriteObject.y/32) == Math.floor((game.input.activePointer.y+game.camera.y)/32)){
                        name = enemies[i].name;
                        hp = enemies[i].health;
                        move = enemies[i].speed;
                        armor = enemies[i].armor;
                        dash = Math.floor (enemies[i].speed/2);
                        if(nametext != null){
                            nametext.destroy()
                        }
                        if(hptext != null){
                            hptext.destroy()
                        }
                        if(armortext != null){
                            armortext.destroy()
                        }
                        if(movetext != null){
                            movetext.destroy()
                        }
                        if(dashtext != null){
                            dashtext.destroy()
                        }
                        nametext = game.add.text(690,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
                        hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
                        armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
                        movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
                        dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

                        nametext.fixedToCamera = true;
                        hptext.fixedToCamera = true;
                        armortext.fixedToCamera = true;
                        movetext.fixedToCamera = true;
                        dashtext.fixedToCamera = true;

                        selectedunit = name;

                        prepbutton.frame = 1;
                        dashbutton.frame = 1;
                        blastbutton.frame = 1;

                        blastbutton.input.enabled=false;
                        dashbutton.input.enabled=false;
                        prepbutton.input.enabled=false;
                    }
                }

                //player1.x = game.input.activePointer.position.x + this.game.camera.x;
                //player1.y = game.input.activePointer.position.y + this.game.camera.y;
                /*if(backgroundlayer.getTileX(game.input.activePointer.worldX)<20){
                  marker.x = backgroundlayer.getTileX(game.input.activePointer.worldX) * 32;
                  marker.y = backgroundlayer.getTileY(game.input.activePointer.worldY) * 32;
                }*/
            }
            else if (game.input.mouse.button==2){
                if(backgroundlayer.getTileX(game.input.activePointer.worldX)<20){
                  marker.x = backgroundlayer.getTileX(game.input.activePointer.worldX) * 32;
                  marker.y = backgroundlayer.getTileY(game.input.activePointer.worldY) * 32;
                }
                for(var i=0; i<characters.length; i++){
                    if(selectedunit == characters[i].name){
                        characters[i].targetX = Math.floor(game.input.activePointer.worldX/32);
                        characters[i].targetY = Math.floor(game.input.activePointer.worldY/32);
                    }
                }
            }
        }
        else if(phase == "act"){
            act(0);
            //shoot(characters[0].spriteObject.x, characters[0].spriteObject.y, characters[0].targetX, characters[0].targetY, characters[0].spriteObject);
            phase = "Decision"
        }
        if(this.P.isDown){
          pause();
        }
    },


    findObjectsByType: function(type, findmap, layer) {
    var result = new Array();
    findmap.objects[layer].forEach(function(element){
      if(element.properties.type === type) {
        //Phaser uses top left, Tiled bottom left so we have to adjust the y position
        //also keep in mind that the cup images are a bit smaller than the tile which is 16x16
        //so they might not be placed in the exact pixel position as in Tiled
        element.y -= findmap.tileHeight;
        result.push(element);
      }      
    });
    return result;
  },


  createFromTiledObject: function(element, group) {
    var sprite = group.create(element.x, element.y, element.properties.sprite);
 
      //copy all properties to the sprite
      Object.keys(element.properties).forEach(function(key){
        sprite[key] = element.properties[key];
      });
  },

    createButton:function(game, string, x ,y , w,h, callback){

        var button = game.add.button(x,y, 'endturn', callback, this, 2 ,1,0);
        button.fixedToCamera = true; 

        button.anchor.setTo(0.5, 0.5);

        button.width = w;
        button.height = h;

        var buttontext = game.add.text(button.x, button.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});
        buttontext.fixedToCamera = true;

        buttontext.anchor.setTo(0.5, 0.5); 
        return button;

    },
    createButton2:function(game, string, x ,y , w,h, callback){

        pausebutton = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);
        pausebutton.fixedToCamera = true;

        pausebutton.anchor.setTo(0.5, 0.5);

        pausebutton.width = w;
        pausebutton.height = h;

        var buttontext = game.add.text(pausebutton.x, pausebutton.y, string, {font:"24px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    },
    /*pause:function(){

          game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'Paused', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(215+game.camera.x,435+game.camera.y,'Resume', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

          /*resumebutton = this.createButton2(game, "Resume", 260, 450, 200, 80, 
          function(){
          });

          exitbutton = this.createButton2(game, "Main Menu", 520, 450, 200, 80, 
          function(){
          });

          game.input.onDown.add(unpause, this);
          game.paused = true;

    },
    unpause:function(event){
        if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
                game.paused = false;
                pausetext.destroy();
                pausebackground.destroy();
                exitbutton.destroy();
                resumebutton.destroy();
                resumetext.destroy();
                exittext.destroy();
                

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                this.state.start('MainMenu');
            }
          }
        }
    }*/
    


};