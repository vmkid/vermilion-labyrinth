var game = new Phaser.Game(800, 600, Phaser.AUTO, '');

game.state.add('Boot', game.Boot);
game.state.add('Preloader', game.Preloader);
game.state.add('MainMenu', game.MainMenu);
game.state.add('Level1', game.Level1);

game.state.start('Boot');