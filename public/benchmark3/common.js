var characters = [];
var enemies = [];

var selectedunit = "";
var bulletgroup;
var bullet;
var shooter;

var opentil = [];
var closedtil = [];
var moveScore=0;
var openTiles;
var closedTiles;
var pathTiles;

var tween;
var tweenXarr;
var tweenYarr;
var tweenAnglearr;
var tweenPoint;
var speed = 50;

var soundDamageArmor;
var soundDamageFlesh;
var soundWalk;
var soundWhoosh;
var soundBGM1;
var shield = [];


function pause(){
    game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'Paused', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(215+game.camera.x,435+game.camera.y,'Resume', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

        

          game.input.onDown.add(unpause, this);
          game.paused = true;
}

function unpause(event){
    if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
                game.paused = false;
                pausetext.destroy();
                pausebackground.destroy();
                exitbutton.destroy();
                resumebutton.destroy();
                resumetext.destroy();
                exittext.destroy();
                

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                game.state.start('MainMenu');
            }
          }
        }
}

function initalizeLevel(level){
    if(level == 1){
		soundBGM1.play();
        characters = [];
        enemies = [];
        characters.push({ name:'Andrew', health:100, id:'tank1', armor:140, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tempsprite', action:0, finished:false, targetX:0, targetY:0});
        characters.push({ name:'Bryan', health:70, id: 'damage1', armor:60, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tempsprite', action:0, finished:false, targetX:0, targetY:0});
        characters.push({ name:'Zach', health:90, id:'support1', armor:90, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tempsprite', action:0, finished:false, targetX:0, targetY:0});
        enemies.push({ name:'enemy1', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tempsprite', action:0, finished:false, targetX:0, targetY:0});
        enemies.push({ name:'enemy2', health:200, armor:200, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tempsprite', action:0, finished:false, targetX:0, targetY:0});
        
    }   
    else if(level == 2){

    }
    else if(level == 3){

    }
}

function addShield(x, y, position){
	shield[position] = game.add.graphics();
    shield[position].lineStyle(2, 0x9900FF, 1);
    shield[position].drawRect(x, y, 32, 32);
}

function removeShield(position){
	shield[position].destroy();
}

function shoot(targetx, targety, shooterSprite){

	//set character that shot
	shooter = shooterSprite;
    //these are to get the center of the tiles
    var x = shooter.body.x*32 + 16;
    var y = shooter.body.y*32 + 16;
    targetx = targetx*32 + 16;
    targety = targety*32 + 16;

    var initPoint = new Phaser.Point(x,y);
    var endPoint = new Phaser.Point(targetx,targety);

    bullet = game.add.sprite(x, y, 'bullet');
    game.physics.arcade.enable(bullet);
    bulletgroup = game.add.group();
    bulletgroup.add(bullet);
    game.world.bringToTop(bulletgroup);


    //velocity calculation
    //dist = a = (a^2 + b^2)^1/2
    var dist = Math.sqrt(Math.pow(Math.abs(x - targetx) + Math.abs(y - targety),2));
    var time = dist/500;

    var xVelocity = (-1)*(x - targetx)/time;
    var yVelocity = (-1)*(y - targety)/time;

    bullet.body.velocity.x = xVelocity;
    bullet.body.velocity.y = yVelocity;

    
}

function bulletCollision(bulletSprite, characterSprite){
    bulletSprite.kill;
    for(var i=0; i<characters.length; i++){
        if(characters[i].spriteObject == characterSprite && shooter != characters[i].spriteObject){
            dealDamage("character",i);
			bullet.kill();
        }
    }
    for(var i=0; i<enemies.length; i++){
        if(enemies[i].spriteObject == characterSprite){
            dealDamage("enemy", i);
			bullet.kill();
        }
    }
}

function bulletMiss(bulletSprite, blocked){
    bulletSprite.kill();
	act(3);
}

function dealDamage(unit, charnum){
	soundDamageFlesh.play();
	if(unit == "character"){
		//will calculate damage later
		characters[charnum].health -=10;
	}
	else{
		enemies[charnum].health -= 10;
	}
	act(3);
}

function act(action){
    if(action==4){
        //Do end of move stuff if we reach this point.
		endturn.input.enabled = true;
        blastbutton.input.enabled=true;
        dashbutton.input.enabled=true;
        prepbutton.input.enabled=true;
        return;
    }
    var actor;
    //Start with a combined array of characters and enemies
    var charEnem = characters.concat(enemies);
    //Iterate until we find a character that's performing this move.
    for(var i=0; i<charEnem.length; i++){
        if(charEnem[i].action==action && !charEnem[i].finished){
            actor=charEnem[i];
        }
    }

    if(actor==null){
        act(action+1);
    }
	else{
		actor.finished=true;
		if (action == 1) {
			//Perform 1st action with actor
			//Temp code
			act(1);
		}
		else if (action == 2) {
			pathfinding(actor.targetX, actor.targetY, actor.spriteObject);
		}
		else if (action == 3) {
			shoot(actor.targetX, actor.targetY, actor.spriteObject);
		}
	}
}

function pathfinding(inputX, inputY, charSprite){
    //if(map.getTile(inputX, inputY, layer).properties.solid==false){
				// openTiles.removeAll(true);
				// closedTiles.removeAll(true);
				// pathTiles.removeAll(true);
				opentil = [];
				closedtil = [];
				pathtil=[];
				opentil[0] = { x: Math.round(charSprite.body.x / 32), y: Math.round(charSprite.body.y / 32), g: 0, h: 0, parent: null };
				opentil[0].h = Math.abs(10 * (Math.round((opentil[0].x - game.input.x) + (opentil[0].y - game.input.y))));
				var foundPath = false;
				var currentPathSquare;
				do {
					//Find lowest f on the open list
					opentil.sort(function (a, b) { return (b.g + b.h) - (a.g + a.h) });
					//Move it to the closed list
					var currentSquare = opentil.pop();
					closedtil.push(currentSquare);
					//Look at the 8 squares around the current one
					for (var x = currentSquare.x - 1; x <= currentSquare.x + 1; x++) {
						for (var y = currentSquare.y - 1; y <= currentSquare.y + 1; y++) {
							//Ignore if it's blocked, or on the Closed List.
							var onClosed = false;
							if(map.getTile(x, y, backgroundlayer).properties.solid==true){
								onClosed=true;
							}
							else{
							for (var i = 0; i < closedtil.length; i++) {
								if (closedtil[i].x == x && closedtil[i].y == y) {
									onClosed = true;
								}
							}
							}
							if (!onClosed) {
								var onOpen = false;
								var sq;
								for (var i = 0; i < opentil.length; i++) {
									if (opentil[i].x == x && opentil[i].y == y) {
										onOpen = true;
										sq = opentil[i];
									}
								}
								//If it's on the open list...
								if (onOpen) {
									//Recalculate its G and F based on current square
									var addAmt = 10;
									if (sq.x != currentSquare.x && sq.y != currentSquare.y) {
										addAmt = 14;
									}
									sq.parent = currentSquare;
									sq.g = currentSquare.g + addAmt;
								}
								//If not...
								else {
									//Add it to the Open list. Calculate F, G, and H based on current square
									var newSq = { x: x, y: y, g: 0, h: 0, parent: currentSquare };
									//newSq.h = Math.abs(10*(Math.round((newSq.x - inputX)+(newSq.y - inputY))));
									newSq.h = 10 * Math.round(Math.abs(newSq.x - inputX) + Math.abs(newSq.y - inputY));
									if (x != currentSquare.x && y != currentSquare.y) {
										newSq.g = currentSquare.g + 10;
									}
									else {
										newSq.g = currentSquare.g + 14;
									}
									opentil.push(newSq);
								}
							}
						}
					}
					//Check if the target square is on the closed list, and set foundPath if we have.
					if (currentSquare.x == inputX && currentSquare.y == inputY) {
						foundPath = true;
						currentPathSquare = currentSquare;
					}
				} while (!foundPath);
				while (currentPathSquare.parent !== null) {
					//pathTiles.create(currentPathSquare.x * 32, currentPathSquare.y * 32, 'path');
					pathtil.push(currentPathSquare);
					currentPathSquare = currentPathSquare.parent;
				}
				var i = pathtil.length;
				tween = game.add.tween(charSprite);
				tweenXarr = [];
				tweenYarr = [];
				tweenAnglearr=[];
				tween.onComplete.add(tweenComplete, this);
				while (i--) {
					var tile = pathtil[i];
					var ang = Math.atan2(tile.y-tweenYarr[tweenYarr.length-1], tile.x-tweenXarr[tweenXarr.length-1])*180/Math.PI;
					tweenAnglearr.push(ang);
					tweenXarr.push(tile.x);
					tweenYarr.push(tile.y);
				}
				tweenAnglearr.splice(0, 1);
				tween.to({ x: tweenXarr, y: tweenYarr, angle: tweenAnglearr }, speed * tweenXarr.length);
				if (tween !== null) {
					tween.start();
				}
				tweenPoint=0;
				soundWalk.play();
				//charSprite.animations.play('walk');

			//}
}

function tweenComplete(){
	soundWalk.stop();
	act(2);
}

function initSounds(){
	soundDamageArmor = game.add.audio('damagearmor');
	soundDamageFlesh = game.add.audio('damageflesh');
	soundWalk = game.add.audio('walk');
	soundWalk.loop=true;
	soundWhoosh = game.add.audio('whoosh');
	soundBGM1 = game.add.audio('bgm1');
	soundBGM1.loop=true;

	//While we're here we'll also initialize the pathfinding groups. Because why the fuck not?
	openTiles=game.add.group();
	closedTiles=game.add.group();
	pathTiles=game.add.group();
	this.game=game;    
}