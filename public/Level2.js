Game.Level2 = function (game){

var cursors;
var marker;
var marker2;

//var player1;
var enemy1;
var enemy2;
var units;

var phase = 'Decision';
//var phasetext;

var ui;
var infoborder;
var overlay;
var pausebutton;

var backgorundlayer;
var blockedlayer;

var pausegroup;
var pausetext;
var resumebutton;
var resumetext;
var exitbutton;
var exittext;
var errortext;
var pausebackground;

var endturn;
var blastbutton;
var dashbutton;
var prepbutton;
var movebutton;
var armorshotbutton;

//hotkeys
var pKey;
var iKey;
var key1;
var key2;
var key3;

var name = '';
var nametext;
var hp;
var hptext;
var armor;
var armortext;
var move; 
var movetext;
var dash;
var dashtext;

var map;
var lastinvalid;


};

Game.Level2.prototype = {

    create:function(){


        initalizeLevel(2);        

        map = this.game.add.tilemap('level2');


        cursors = this.game.input.keyboard.createCursorKeys();
 
        //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        map.addTilesetImage('level1tileset', 'level1-tileset');
    
        //create layer
        backgroundlayer = map.createLayer('Tile Layer 1');
        blockedlayer = map.createLayer('blockedlayer');
        game.physics.arcade.enable(blockedlayer);
    

        setBlocked(blockedlayer);
        setBackground(backgroundlayer);
        //collision on blockedLayer
        map.setCollisionBetween(1, 2000, true, 'blockedlayer');
    
        //resizes the game world to match the layer dimensions
        backgroundlayer.resizeWorld();

        units = this.findObjectsByType('spawn', map, 'spawnlocations');

 
        //we know there is just one result
        //player1 = this.game.add.sprite(units[0].x, units[0].y, 'tempsprite');
        /*characters[1].spriteObject = this.game.add.sprite(units[4].x, units[4].y, characters[1].spriteSheet);
        characters[2].spriteObject = this.game.add.sprite(units[3].x, units[3].y, characters[2].spriteSheet);
        characters[0].spriteObject = this.game.add.sprite(units[2].x, units[2].y, characters[0].spriteSheet);
        enemies[1].spriteObject = this.game.add.sprite(units[1].x, units[1].y, 'tempsprite');
        enemies[0].spriteObject = this.game.add.sprite(units[0].x, units[0].y, 'tempsprite');*/

        characters[1].spriteObject = null;
        characters[2].spriteObject = null;
        characters[0].spriteObject = null;
        enemies[1].spriteObject = null;
        enemies[0].spriteObject = null;
        //enemies[2].spriteObject = null;

        
        characters[1].spriteObject = this.game.add.sprite(units[1].x, units[1].y, characters[1].spriteSheet);
        characters[2].spriteObject = this.game.add.sprite(units[2].x, units[2].y, characters[2].spriteSheet);
        characters[0].spriteObject = this.game.add.sprite(units[0].x, units[0].y, characters[0].spriteSheet);
        enemies[2].spriteObject = this.game.add.sprite(units[5].x, units[5].y, 'enemy2');
        enemies[3].spriteObject = this.game.add.sprite(units[6].x, units[6].y, 'enemy2');
        enemies[4].spriteObject = this.game.add.sprite(units[7].x, units[7].y, 'enemy2');
        enemies[1].spriteObject = this.game.add.sprite(units[4].x, units[4].y, 'enemy2');
        enemies[0].spriteObject = this.game.add.sprite(units[3].x, units[3].y, 'enemy2');
        //enemies[1].spriteObject = this.game.add.sprite(units[5].x, units[5].y, 'tempsprite');
        
        
        game.physics.arcade.enable(characters[0].spriteObject);
        game.physics.arcade.enable(characters[1].spriteObject);
        game.physics.arcade.enable(characters[2].spriteObject);
        game.physics.arcade.enable(enemies[0].spriteObject);
        game.physics.arcade.enable(enemies[1].spriteObject);
        game.physics.arcade.enable(enemies[2].spriteObject);
        game.physics.arcade.enable(enemies[3].spriteObject);
        game.physics.arcade.enable(enemies[4].spriteObject);


        characters[0].spriteObject.animations.add('idle',[0,1,2], 6,true);
        characters[0].spriteObject.animations.add('up',[3,4,5], 6,true);
        characters[0].spriteObject.animations.add('left',[6,7,8], 6,true);
        characters[0].spriteObject.animations.add('down',[9,10,11], 6,true);
        characters[0].spriteObject.animations.add('right',[12,13,14], 6,true);
        characters[0].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        characters[0].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        characters[0].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        characters[0].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        characters[0].spriteObject.animations.add('die',[27,28,29], 6,true);

        characters[1].spriteObject.animations.add('idle',[0,1,2], 6,true);
        characters[1].spriteObject.animations.add('up',[3,4,5], 6,true);
        characters[1].spriteObject.animations.add('left',[6,7,8], 6,true);
        characters[1].spriteObject.animations.add('down',[9,10,11], 6,true);
        characters[1].spriteObject.animations.add('right',[12,13,14], 6,true);
        characters[1].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        characters[1].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        characters[1].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        characters[1].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        characters[1].spriteObject.animations.add('die',[27,28,29], 6,true);

        characters[2].spriteObject.animations.add('idle',[0,1,2], 6,true);
        characters[2].spriteObject.animations.add('up',[3,4,5], 6,true);
        characters[2].spriteObject.animations.add('left',[6,7,8], 6,true);
        characters[2].spriteObject.animations.add('down',[9,10,11], 6,true);
        characters[2].spriteObject.animations.add('right',[12,13,14], 6,true);
        characters[2].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        characters[2].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        characters[2].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        characters[2].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        characters[2].spriteObject.animations.add('die',[27,28,29], 6,true);

        enemies[0].spriteObject.animations.add('idle',[0,1,2], 6,true);
        enemies[0].spriteObject.animations.add('up',[3,4,5], 6,true);
        enemies[0].spriteObject.animations.add('left',[6,7,8], 6,true);
        enemies[0].spriteObject.animations.add('down',[9,10,11], 6,true);
        enemies[0].spriteObject.animations.add('right',[12,13,14], 6,true);
        enemies[0].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        enemies[0].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        enemies[0].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        enemies[0].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        enemies[0].spriteObject.animations.add('die',[27,28,29], 6,true);

        enemies[1].spriteObject.animations.add('idle',[0,1,2], 6,true);
        enemies[1].spriteObject.animations.add('up',[3,4,5], 6,true);
        enemies[1].spriteObject.animations.add('left',[6,7,8], 6,true);
        enemies[1].spriteObject.animations.add('down',[9,10,11], 6,true);
        enemies[1].spriteObject.animations.add('right',[12,13,14], 6,true);
        enemies[1].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        enemies[1].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        enemies[1].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        enemies[1].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        enemies[1].spriteObject.animations.add('die',[27,28,29], 6,true);

        enemies[2].spriteObject.animations.add('idle',[0,1,2], 6,true);
        enemies[2].spriteObject.animations.add('up',[3,4,5], 6,true);
        enemies[2].spriteObject.animations.add('left',[6,7,8], 6,true);
        enemies[2].spriteObject.animations.add('down',[9,10,11], 6,true);
        enemies[2].spriteObject.animations.add('right',[12,13,14], 6,true);
        enemies[2].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        enemies[2].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        enemies[2].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        enemies[2].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        enemies[2].spriteObject.animations.add('die',[27,28,29], 6,true);

        enemies[3].spriteObject.animations.add('idle',[0,1,2], 6,true);
        enemies[3].spriteObject.animations.add('up',[3,4,5], 6,true);
        enemies[3].spriteObject.animations.add('left',[6,7,8], 6,true);
        enemies[3].spriteObject.animations.add('down',[9,10,11], 6,true);
        enemies[3].spriteObject.animations.add('right',[12,13,14], 6,true);
        enemies[3].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        enemies[3].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        enemies[3].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        enemies[3].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        enemies[3].spriteObject.animations.add('die',[27,28,29], 6,true);

        enemies[4].spriteObject.animations.add('idle',[0,1,2], 6,true);
        enemies[4].spriteObject.animations.add('up',[3,4,5], 6,true);
        enemies[4].spriteObject.animations.add('left',[6,7,8], 6,true);
        enemies[4].spriteObject.animations.add('down',[9,10,11], 6,true);
        enemies[4].spriteObject.animations.add('right',[12,13,14], 6,true);
        enemies[4].spriteObject.animations.add('m-left',[15,16,17], 6,true);
        enemies[4].spriteObject.animations.add('m-up',[18,19,20], 6,true);
        enemies[4].spriteObject.animations.add('m-right',[21,22,23], 6,true);
        enemies[4].spriteObject.animations.add('m-down',[24,25,26], 6,true);
        enemies[4].spriteObject.animations.add('die',[27,28,29], 6,true);


        //the camera will follow the player in the world
        this.game.camera.follow(this.player);

        pausegroup = game.add.group();
        
        //move player with cursor keys
        this.cursors = this.game.input.keyboard.createCursorKeys();


        //ui
        ui = game.add.group();

        overlay = ui.create(640,0, 'overlay');
        infoborder = ui.create(648,312, 'charinfo');
        infoborder.fixedToCamera = true;
        overlay.fixedToCamera = true;

        //pausebutton

        //Command buttons
        endturn=this.createButton(game, "End Turn", 724, 574, 146, 40, 
        function(){
          //will trigger action phase sequence
          endenabled = false;
          endturn.input.enabled = false;
          blastbutton.input.enabled=false;
          dashbutton.input.enabled=false;
          //.input.enabled=false;
          endturn.frame = 0;
          lastinvalid = -1;
          lastselfshot = -1;
          lastselfmove = -1;
          var samespot = false;
          //checks that every character selected an action
          //needs to be tested
          if(characters[0].action != 0 && characters[1].action != 0 && characters[2].action != 0){
            //checks that every character has a target location

            if(characters[0].targetX != -1 && characters[1].targetX != -1 && characters[2].targetX != -1){
                //checks that if character is moving or dashing it isnt trying to go too far

                var validmoves = true;
                var shootself = false;
                var moveself = false;
                for(var i=0; i<characters.length; i++){
                    if(characters[i].action ==2){
                        //make method calculateDistance, if distance >5 invalid move and have message pop up
                        var distance = calculateDistance(characters[i]);
                        if(characters[i].speed/2 <distance){
                            validmoves = false;
                            lastinvalid = i;
                        }
                    }
                    else if(characters[i].action == 3){
                        if(characters[i].targetX-Math.round(characters[i].spriteObject.x/32)==0){
                            if(characters[i].targetY-Math.round(characters[i].spriteObject.y/32)==0){
                                shootself = true;
                                lastselfshot = i;
                            }
                        }
                    }
                    else if(characters[i].action ==4){
                        //make method calculateDistance, if distance >5 invalid move and have message pop up
                        var distance = calculateDistance(characters[i]);
                        if(characters[i].speed <distance){
                            validmoves = false;
                            lastinvalid = i;
                        }
                        else if(characters[i].targetX-Math.round(characters[i].spriteObject.x/32)==0){
                            if(characters[i].targetY-Math.round(characters[i].spriteObject.y/32)==0){
                                moveself = true;
                                lastselfmove = i;
                            }
                        }
                    }
                }
                if(lastinvalid>-1){
                            //error message if all actions not set
                            if(errortext != null){
                                errortext.visible = false;
                            }
                            errortext = game.add.text(140,20,characters[lastinvalid].name+'\'s selected location is out of range', {font: '20px Arial', fill:'#fff', align:'center'}); 
                            errortext.fixedToCamera = true;
                            //removes message after 2 seconds
                            game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                                    errortext.visible = false;
                            }, this);
                            endturn.input.enabled = true;
                        }
                else if(shootself == true){
                    if(errortext != null){
                        errortext.visible = false;
                    }
                    errortext = game.add.text(180,20,characters[lastselfshot].name+' cannot aim at himself', {font: '20px Arial', fill:'#fff', align:'center'}); 
                            errortext.fixedToCamera = true;
                            //removes message after 2 seconds
                            game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                                    errortext.visible = false;
                            }, this);
                            endturn.input.enabled=true;
                }
                else if(moveself == true){
                    if(errortext != null){
                        errortext.visible = false;
                    }
                    errortext = game.add.text(180,20,characters[lastselfmove].name+' cannot move to the current tile', {font: '20px Arial', fill:'#fff', align:'center'}); 
                            errortext.fixedToCamera = true;
                            //removes message after 2 seconds
                            game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                                    errortext.visible = false;
                            }, this);
                            endturn.input.enabled=true;
                }
                else if(checkLocations() == true){
                    if(errortext != null){
                        errortext.visible = false;
                    }
                    errortext = game.add.text(120,20,'Cannot have two units end at the same location', {font: '20px Arial', fill:'#fff', align:'center'}); 
                            errortext.fixedToCamera = true;
                            //removes message after 2 seconds
                            game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                                    errortext.visible = false;
                            }, this);
                            endturn.input.enabled=true;
                            samespot = true;
                }

                if(validmoves == true && shootself == false && moveself ==false && samespot == false){
                    phase = "act";
                   // act(1);
                }
            }
            else{
                //error message if all targets not set
                errortext = game.add.text(160,20,'Every Character must select a target tile', {font: '20px Arial', fill:'#fff', align:'center'}); 
                errortext.fixedToCamera = true;
                //removes message after 2 seconds
                game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                    errortext.visible = false;
               }, this);
               endturn.input.enabled=true;
            }
          }
          else{
              //error message if all actions not set
               errortext = game.add.text(160,20,'Every Character must select an action', {font: '20px Arial', fill:'#fff', align:'center'}); 
               errortext.fixedToCamera = true;
              //removes message after 2 seconds
               game.time.events.add(Phaser.Timer.SECOND * 2, function(){
                    errortext.visible = false;
               }, this);
               endturn.input.enabled=true;
          }
        });
        //endturn.input.enabled=false;

        blastbutton=this.createButton(game, "Blast HP", 724, 188, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=false;
          dashbutton.input.enabled=true;
          //.input.enabled=true;
          movebutton.input.enabled=true;
          armorshotbutton.input.enabled=true;

          dashbutton.frame = 1;
          //.frame = 1;
          movebutton.frame = 1;
          armorshotbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 3;
                }
          }
          
          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].shotType = 'hp';
                }
          }

        });
        
        dashbutton=this.createButton(game, "Dash", 724, 142, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=false;
          //.input.enabled=true;
          movebutton.input.enabled=true;
          armorshotbutton.input.enabled=true;

          blastbutton.frame = 1;
          //.frame = 1;
          movebutton.frame = 1;
          armorshotbutton.frame = 1;


          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 2;
                }
          }
        });
        /*=this.createButton(game, "Prep", 724, 96, 140, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=true;
          //.input.enabled=false;
          movebutton.input.enabled=true;
          armorshotbutton.input.enabled=true;

          blastbutton.frame = 1;
          dashbutton.frame = 1;
          movebutton.frame = 1;
          armorshotbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 1;
                }
          }

        });*/

        armorshotbutton=this.createButton(game, "Blast armor", 724, 234, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=true;
          //.input.enabled=true;
          movebutton.input.enabled=true;
          armorshotbutton.input.enabled=false;

          dashbutton.frame = 1;
          //.frame = 1;
          movebutton.frame = 1;
          blastbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 3;
                }
          }
          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].shotType = 'armor';
                }
          }

        });

        movebutton=this.createButton(game, "Move", 724, 280, 146, 40,
        function(){
          endturn.input.enabled=true;
          blastbutton.input.enabled=true;
          dashbutton.input.enabled=true;
          //.input.enabled=true;
          movebutton.input.enabled=false;
          armorshotbutton.input.enabled=true;

          dashbutton.frame = 1;
          //.frame = 1;
          armorshotbutton.frame = 1;
          blastbutton.frame = 1;

          for(var i=0; i<characters.length; i++){
                if(characters[i].name == selectedunit){
                    characters[i].action = 4;
                }
          }

        });

        
        phasetext = game.add.text(655,30,'Decision Phase', {font: '20px Arial', fill:'#fff', align:'center'}); 
        phasetext.fixedToCamera = true;

        //marks clicked tiles
        marker = game.add.graphics();
        marker.lineStyle(2, 0xffffff, 1);
        marker.drawRect(0, 0, 32, 32);

        marker2 = game.add.graphics();
        marker2.lineStyle(2, 0xffffff, 1);
        marker2.drawRect(0, 0, 32, 32);

        


        //set camera over players characters
        game.camera.y = 1000;
        game.camera.x = 150;

        //set hotkeys
        this.P = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.I = game.input.keyboard.addKey(Phaser.Keyboard.I);
        key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);

        //disable menu on right clicke
        game.canvas.oncontextmenu = function (e) { e.preventDefault();}

    },

    update:function(){

        checkDeaths();

        if(selectedmoved == true){
            selectedmoved = false;
            //check if selected moved, if so move marker
            for(var i=0; i<characters.length; i++){
                if(selectedunit == characters[i].name){
                    marker2.visible = false
                    marker.visible = false;
                }
            }
            for(var i=0; i<enemies.length; i++){
                if(selectedunit == enemies[i].name){
                    marker2.visible = false
                    marker.visible = false;
                }
            }
        }
        
        fixStats();

        //first checks player characters
        var unitnum = -1;
        for(var i=0; i<characters.length; i++){
            if(selectedunit == characters[i].name){
                unitnum = i;
            }
        }
        if(unitnum>-1){
            name = characters[unitnum].name;
            hp = characters[unitnum].health;
            move = characters[unitnum].speed;
            armor = characters[unitnum].armor;
            dash = Math.floor (characters[unitnum].speed/2);
            if(nametext != null){
                nametext.destroy()
            }
            if(hptext != null){
                hptext.destroy()
            }
            if(armortext != null){
                armortext.destroy()
            }
            if(movetext != null){
                movetext.destroy()
            }
            if(dashtext != null){
                dashtext.destroy()
            }
            nametext = game.add.text(700,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
            hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
            armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
            movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
            dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

            nametext.fixedToCamera = true;
            hptext.fixedToCamera = true;
            armortext.fixedToCamera = true;
            movetext.fixedToCamera = true;
            dashtext.fixedToCamera = true;

            if(characters[unitnum].isDead == true){
                marker.visible = false;
                marker2.visible = false;
            }
        }


        //next checks enemies
        var unitnum = -1;
        for(var i=0; i<enemies.length; i++){
            if(selectedunit == enemies[i].name){
                unitnum = i;
            }
        }
        if(unitnum>-1){
            name = enemies[unitnum].name;
            hp = enemies[unitnum].health;
            move = enemies[unitnum].speed;
            armor = enemies[unitnum].armor;
            dash = Math.floor (enemies[unitnum].speed/2);
            if(nametext != null){
                nametext.destroy()
            }
            if(hptext != null){
                hptext.destroy()
            }
            if(armortext != null){
                armortext.destroy()
            }
            if(movetext != null){
                movetext.destroy()
            }
            if(dashtext != null){
                dashtext.destroy()
            }
            nametext = game.add.text(700,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
            hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
            armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
            movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
            dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

            nametext.fixedToCamera = true;
            hptext.fixedToCamera = true;
            armortext.fixedToCamera = true;
            movetext.fixedToCamera = true;
            dashtext.fixedToCamera = true;

            if(enemies[unitnum].isDead == true){
                marker.visible = false;
                marker2.visible = false;
            }
        }


        //lets bullet be killed if out of bounds
        if(bullet!= null){
            if((bullet.x<0 || bullet.y<0 || bullet.x > 960 || bullet.y>1600) && hitDoor == false){
                bullet.kill();
                hitDoor = true;
                act(3);
            }
        }
        //lets bullet collide with wall
        game.physics.arcade.overlap(bullet, blockedlayer, bulletMiss, null, this);
        //lets bullet collide with other characters, will have one for each character
        game.physics.arcade.overlap(bullet, characters[0].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, characters[1].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, characters[2].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[0].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[1].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[2].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[3].spriteObject, bulletCollision, null ,this);
        game.physics.arcade.overlap(bullet, enemies[4].spriteObject, bulletCollision, null ,this);

        //enables or disables end-return
        if(characters[0].action !=0 && characters[1].action !=0 && characters[2].action !=0 && endenabled == true ){
            endturn.input.enabled = true;
            endturn.frame = 1;
        }

        //camera controls
        if(cursors.up.isDown){
          game.camera.y -= 6;
        }
        else if(cursors.down.isDown){
          game.camera.y += 6;
        }
        else if(cursors.left.isDown){
          game.camera.x -= 6;
        }
        else if(cursors.right.isDown){
          game.camera.x += 6;
        }


        if(phase == 'Decision'){
            if(game.input.mouse.button==0){
                for(var i=0; i<characters.length; i++){
                    if(Math.floor(characters[i].spriteObject.x/32) == Math.floor((game.input.activePointer.x+game.camera.x)/32) && Math.floor(characters[i].spriteObject.y/32) == Math.floor((game.input.activePointer.y+game.camera.y)/32) && characters[i].isDead == false){
                        name = characters[i].name;
                        hp = characters[i].health;
                        move = characters[i].speed;
                        armor = characters[i].armor;
                        dash = Math.floor (characters[i].speed/2);
                        if(nametext != null){
                            nametext.destroy()
                        }
                        if(hptext != null){
                            hptext.destroy()
                        }
                        if(armortext != null){
                            armortext.destroy()
                        }
                        if(movetext != null){
                            movetext.destroy()
                        }
                        if(dashtext != null){
                            dashtext.destroy()
                        }
                        nametext = game.add.text(700,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
                        hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
                        armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
                        movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
                        dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

                        nametext.fixedToCamera = true;
                        hptext.fixedToCamera = true;
                        armortext.fixedToCamera = true;
                        movetext.fixedToCamera = true;
                        dashtext.fixedToCamera = true;

                        selectedunit = name;

                        if(characters[i].action == 1){
                            //.frame = 0;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;
                            armorshotbutton.frame = 1;
                            movebutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            //.input.enabled=false;
                            armorshotbutton.input.enabled=true;
                            movebutton.input.enabled=true;
                        }
                        else if(characters[i].action == 2){
                            //.frame = 1;
                            dashbutton.frame = 0;
                            blastbutton.frame = 1;
                            armorshotbutton.frame = 1;
                            movebutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=false;
                            //.input.enabled=true;
                            armorshotbutton.input.enabled=true;
                            movebutton.input.enabled=true;
                        }
                        else if(characters[i].action == 3 && characters[i].shotType == 'armor'){
                            //.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;
                            armorshotbutton.frame = 0;
                            movebutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            //.input.enabled=true;
                            armorshotbutton.input.enabled=false;
                            movebutton.input.enabled=true;
                        }
                        else if(characters[i].action == 3 && characters[i].shotType == 'hp'){
                            //.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 0;
                            armorshotbutton.frame = 1;
                            movebutton.frame = 1;

                            blastbutton.input.enabled=false;
                            dashbutton.input.enabled=true;
                            //.input.enabled=true;
                            armorshotbutton.input.enabled=true;
                            movebutton.input.enabled=true;
                        }
                        else if(characters[i].action == 4){
                            //.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;
                            armorshotbutton.frame = 1;
                            movebutton.frame = 0;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            //.input.enabled=true;
                            armorshotbutton.input.enabled=true;
                            movebutton.input.enabled=false;
                        }
                        else{
                            //.frame = 1;
                            dashbutton.frame = 1;
                            blastbutton.frame = 1;
                            armorshotbutton.frame = 1;
                            movebutton.frame = 1;

                            blastbutton.input.enabled=true;
                            dashbutton.input.enabled=true;
                            //.input.enabled=true;
                            armorshotbutton.input.enabled=true;
                            movebutton.input.enabled=true;

                                
                            
                        }
                        marker2.visible = true;
                        marker2.destroy();
                        marker2 = game.add.graphics();
                        marker2.lineStyle(2, 0x23EB10, 1);
                        marker2.drawRect(0, 0, 32, 32);
                        marker2.x = characters[i].spriteObject.x;
                        marker2.y = characters[i].spriteObject.y;
                    }
                }
                for(var i=0; i<enemies.length; i++){
                    if(Math.floor(enemies[i].spriteObject.x/32) == Math.floor((game.input.activePointer.x+game.camera.x)/32) && Math.floor(enemies[i].spriteObject.y/32) == Math.floor((game.input.activePointer.y+game.camera.y)/32)){
                        name = enemies[i].name;
                        hp = enemies[i].health;
                        move = enemies[i].speed;
                        armor = enemies[i].armor;
                        dash = Math.floor (enemies[i].speed/2);
                        if(nametext != null){
                            nametext.destroy()
                        }
                        if(hptext != null){
                            hptext.destroy()
                        }
                        if(armortext != null){
                            armortext.destroy()
                        }
                        if(movetext != null){
                            movetext.destroy()
                        }
                        if(dashtext != null){
                            dashtext.destroy()
                        }
                        nametext = game.add.text(690,340,name, {font: '20px Arial', fill:'#fff', align:'center'});
                        hptext = game.add.text(670,390,"Health: "+ hp, {font: '20px Arial', fill:'#fff', align:'center'});
                        armortext = game.add.text(670,437,"Armor: "+armor, {font: '20px Arial', fill:'#fff', align:'center'});
                        movetext = game.add.text(665,488,"Movement: "+ move, {font: '20px Arial', fill:'#fff', align:'center'});
                        dashtext = game.add.text(685,525,"Dash : "+dash, {font: '20px Arial', fill:'#fff', align:'center'});

                        nametext.fixedToCamera = true;
                        hptext.fixedToCamera = true;
                        armortext.fixedToCamera = true;
                        movetext.fixedToCamera = true;
                        dashtext.fixedToCamera = true;

                        selectedunit = name;

                        //.frame = 1;
                        dashbutton.frame = 1;
                        blastbutton.frame = 1;
                        armorshotbutton.frame = 1;
                        movebutton.frame = 1;

                        blastbutton.input.enabled=false;
                        dashbutton.input.enabled=false;
                        //.input.enabled=false;
                        armorshotbutton.input.enabled=false;
                        movebutton.input.enabled=false;

                        marker2.visible = true;
                        marker2.destroy();
                        marker2 = game.add.graphics();
                        marker2.lineStyle(2, 0xDE0404, 1);
                        marker2.drawRect(0, 0, 32, 32);
                        marker2.x = enemies[i].spriteObject.x;
                        marker2.y = enemies[i].spriteObject.y;
                    }     
                }


                //remove box if nothing selected or if enemy selected
                //move box if target is different
                for(var i=0; i<characters.length;i++){
                    if(selectedunit == characters[i].name){
                        marker.visible = true;
                        if(characters[i].targetX != -1){
                            marker.x = characters[i].targetX*32;
                            marker.y = characters[i].targetY*32;
                        }
                        else{
                            marker.visible = false;
                        }
                    }
                }
                for(var i=0; i<enemies.length;i++){
                    if(selectedunit == enemies[i].name){
                        marker.visible = false;
                    }
                }
            }
            else if (game.input.mouse.button==2){
                var isPlayer = false;
                for(var i =0; i<characters.length; i++){
                    if(characters[i].name == selectedunit){
                        isPlayer = true;
                    }
                }
                if(isPlayer == true){
                    if(backgroundlayer.getTileX(game.input.activePointer.worldX)<30){
                    marker.visible = true;
                    marker.x = backgroundlayer.getTileX(game.input.activePointer.worldX) * 32;
                    marker.y = backgroundlayer.getTileY(game.input.activePointer.worldY) * 32;
                    }
                    for(var i=0; i<characters.length; i++){
                        if(selectedunit == characters[i].name){
                            characters[i].targetX = Math.floor(game.input.activePointer.worldX/32);
                            characters[i].targetY = Math.floor(game.input.activePointer.worldY/32);
                        }
                    }
                }
            }
        }
        else if(phase == "act"){
            phasetext.setText('Action Phase');
            ai(2);
            act(1);
            phase = "Decision"
        }
        if(this.P.isDown){
          pause();
        }
        else if(this.I.isDown){
          invincible();
        }
        else if(key1.isDown){
            game.camera.x=0;
            game.camera.y=0;
            this.state.start('Level1');
        }
        else if(key2.isDown){
            game.camera.x=0;
            game.camera.y=0;
            this.state.start('Level2');
        }
        else if(key3.isDown){
            game.camera.x=0;
            game.camera.y=0;
            this.state.start('Level3');
        }


         //end level if all enemies are dead or if all player characters
        var alldead = true;
        for(var i =0; i<characters.length; i++){
            if(characters[i].isDead == false){
                alldead = false;
            }
        }
        if(alldead == true){
            loseLevel();
        }
        var alldead = true;
        for(var i =0; i<enemies.length; i++){
            if(enemies[i].isDead == false){
                alldead = false;
            }
        }
        if(alldead == true){
            winLevel();
        }

        if(tweenReady){
            tween = game.add.tween(tweenSprite);
            tweenXarr = [];
            tweenYarr = [];
            //tweenAnglearr = [];
            tween.onComplete.add(tweenComplete, this);
            var ownTile=false;
            if(tweenI==0){
                ownTile=true;
            }
            while (tweenI--) {
                var tile = pathtil[tweenI];
                //var ang = Math.atan2(tile.y - tweenYarr[tweenYarr.length - 1], tile.x - tweenXarr[tweenXarr.length - 1]) * 180 / Math.PI;
                //tweenAnglearr.push(ang);
                tweenXarr.push(tile.x);
                tweenYarr.push(tile.y);
            }
            //tweenAnglearr.splice(0, 1);
            if(!ownTile){
                tween.to({ x: tweenXarr, y: tweenYarr }, speed * tweenXarr.length, "Linear");
                if (tween !== null) {
                    tween.start();
                }
                tweenPoint = 0;
                soundWalk.play();
            }
            tweenReady=false;
            if(ownTile){
                tweenComplete;
            }
        }


    },


    findObjectsByType: function(type, findmap, layer) {
    var result = new Array();
    findmap.objects[layer].forEach(function(element){
      if(element.properties.type === type) {
        //Phaser uses top left, Tiled bottom left so we have to adjust the y position
        //also keep in mind that the cup images are a bit smaller than the tile which is 16x16
        //so they might not be placed in the exact pixel position as in Tiled
        element.y -= findmap.tileHeight;
        result.push(element);
      }      
    });
    return result;
  },


  createFromTiledObject: function(element, group) {
    var sprite = group.create(element.x, element.y, element.properties.sprite);
 
      //copy all properties to the sprite
      Object.keys(element.properties).forEach(function(key){
        sprite[key] = element.properties[key];
      });
  },

    createButton:function(game, string, x ,y , w,h, callback){

        var button = game.add.button(x,y, 'endturn', callback, this, 2 ,1,0);
        button.fixedToCamera = true; 

        button.anchor.setTo(0.5, 0.5);

        button.width = w;
        button.height = h;

        var buttontext = game.add.text(button.x, button.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});
        buttontext.fixedToCamera = true;

        buttontext.anchor.setTo(0.5, 0.5); 
        return button;

    },
    createButton2:function(game, string, x ,y , w,h, callback){

        pausebutton = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);
        pausebutton.fixedToCamera = true;

        pausebutton.anchor.setTo(0.5, 0.5);

        pausebutton.width = w;
        pausebutton.height = h;

        var buttontext = game.add.text(pausebutton.x, pausebutton.y, string, {font:"24px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    },
    removeMessage:function(){
        errortext.visible = false;
    }

};