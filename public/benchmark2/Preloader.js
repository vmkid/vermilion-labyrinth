Game.Preloader = function(game){

    this.preloadBar = null;
};

Game.Preloader.prototype = {
    preload:function(){

        

        var menuText;

        
        this.load.spritesheet('overlay', 'assets/sidebar.png', 160,800);
        this.load.spritesheet('charinfo', 'assets/infoborder.png', 152,240);
        this.load.spritesheet('menubutton', 'assets/MenuButton.png', 140, 60);
        this.load.spritesheet('pause', 'assets/pausebutton.png', 146, 40);
        this.load.spritesheet('endturn', 'assets/endturn-button.png', 146, 40);
        this.load.spritesheet('tempsprite', 'assets/tempsprite.png', 32, 32);
        

        this.load.tilemap('level1', 'assets/level1-tilemap.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('level1-tileset', 'assets/level1tileset.png');
        this.load.image('pausemenu', 'assets/pause-background.png');
        this.load.image('pausedbutton', 'assets/pausedbutton.png');
    },

    create:function(){
        this.state.start('MainMenu');
    }
};

