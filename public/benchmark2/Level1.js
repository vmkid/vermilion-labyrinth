Game.Level1 = function(game){};

var cursors;
var marker;

var player1;
var enemy1;
var enemy2;
var units;

var phase = 'Decision';
var phasetext;

var ui;
var infoborder;
var overlay;
var pausebutton;

var backgorundlayer;
var blockedlayer;

var pausegroup;
var pausetext;
var resumebutton;
var resumetext;
var exitbutton;
var exittext;
var pausebackground;

var endturn;
var blastbutton;
var dashbutton;
var prepbutton;

//hotkeys
var pKey;



Game.Level1.prototype = {
    create:function(){
        

        this.map = this.game.add.tilemap('level1');


        cursors = this.game.input.keyboard.createCursorKeys();
 
        //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        this.map.addTilesetImage('level1tileset', 'level1-tileset');
    
        //create layer
        backgroundlayer = this.map.createLayer('level1');
        blockedLayer = this.map.createLayer('blockedlayer');
    
        //collision on blockedLayer
        this.map.setCollisionBetween(1, 2000, true, 'blockedlayer');
    
        //resizes the game world to match the layer dimensions
        backgroundlayer.resizeWorld();

        units = this.findObjectsByType('spawn', this.map, 'spawnLocations');
 
        //we know there is just one result
        player1 = this.game.add.sprite(units[0].x, units[0].y, 'tempsprite');
        enemy1 = this.game.add.sprite(units[1].x, units[1].y, 'tempsprite');
        enemy2 = this.game.add.sprite(units[2].x, units[2].y, 'tempsprite');
        
        //the camera will follow the player in the world
        this.game.camera.follow(this.player);

        pausegroup = game.add.group();
        
        //move player with cursor keys
        this.cursors = this.game.input.keyboard.createCursorKeys();


        //ui
        ui = game.add.group();

        overlay = ui.create(640,0, 'overlay');
        infoborder = ui.create(648,312, 'charinfo');
        infoborder.fixedToCamera = true;
        overlay.fixedToCamera = true;

        //pausebutton

        //Command buttons
        endturn=this.createButton(game, "End Turn", 724, 574, 146, 40, 
        function(){
          //will trigger action phase sequence
          endturn.inputEnabled = false;
          blastbutton.inputEnabled=true;
          dashbutton.inputEnabled=true;
          prepbutton.inputEnabled=true;
        });
        endturn.inputEnabled=false;

        blastbutton=this.createButton(game, "Blast", 724, 100, 146, 40,
        function(){
          endturn.inputEnabled=true;
          blastbutton.inputEnabled=false;
          dashbutton.inputEnabled=true;
          prepbutton.inputEnabled=true;

        });
        
        dashbutton=this.createButton(game, "Dash", 724, 150, 146, 40,
        function(){
          endturn.inputEnabled=true;
          blastbutton.inputEnabled=true;
          dashbutton.inputEnabled=false;
          prepbutton.inputEnabled=true;
        });
        prepbutton=this.createButton(game, "Prep", 724, 200, 140, 40,
        function(){
          endturn.inputEnabled=true;
          blastbutton.inputEnabled=true;
          dashbutton.inputEnabled=true;
          prepbutton.inputEnabled=false;
        });

        phasetext = game.add.text(655,30,'Decision Phase', {font: '20px Arial', fill:'#fff', align:'center'}); 
        phasetext.fixedToCamera = true;

        //marks clicked tiles
        marker = game.add.graphics();
        marker.lineStyle(2, 0xffffff, 1);
        marker.drawRect(0, 0, 32, 32);


        //set camera over players characters
        game.camera.y = 200;

        //set hotkeys
        this.P = game.input.keyboard.addKey(Phaser.Keyboard.P);

    },

    update:function(){


        //camera controls
        if(cursors.up.isDown){
          game.camera.y -= 4;
        }
        else if(cursors.down.isDown){
          game.camera.y += 4;
        }


        if(phase == 'Decision'){
            if(game.input.activePointer.isDown){
                //player1.x = game.input.activePointer.position.x + this.game.camera.x;
                //player1.y = game.input.activePointer.position.y + this.game.camera.y;
                if(backgroundlayer.getTileX(game.input.activePointer.worldX)<20){
                  marker.x = backgroundlayer.getTileX(game.input.activePointer.worldX) * 32;
                  marker.y = backgroundlayer.getTileY(game.input.activePointer.worldY) * 32;
                }
            }
        }
        if(this.P.isDown){
          this.pause();
        }
    },


    findObjectsByType: function(type, map, layer) {
    var result = new Array();
    map.objects[layer].forEach(function(element){
      if(element.properties.type === type) {
        //Phaser uses top left, Tiled bottom left so we have to adjust the y position
        //also keep in mind that the cup images are a bit smaller than the tile which is 16x16
        //so they might not be placed in the exact pixel position as in Tiled
        element.y -= map.tileHeight;
        result.push(element);
      }      
    });
    return result;
  },


  createFromTiledObject: function(element, group) {
    var sprite = group.create(element.x, element.y, element.properties.sprite);
 
      //copy all properties to the sprite
      Object.keys(element.properties).forEach(function(key){
        sprite[key] = element.properties[key];
      });
  },

    createButton:function(game, string, x ,y , w,h, callback){

        var button = game.add.button(x,y, 'endturn', callback, this, 2 ,1,0);
        button.fixedToCamera = true; 

        button.anchor.setTo(0.5, 0.5);

        button.width = w;
        button.height = h;

        var buttontext = game.add.text(button.x, button.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});
        buttontext.fixedToCamera = true;

        buttontext.anchor.setTo(0.5, 0.5); 
        return button;

    },
    createButton2:function(game, string, x ,y , w,h, callback){

        pausebutton = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);
        pausebutton.fixedToCamera = true;

        pausebutton.anchor.setTo(0.5, 0.5);

        pausebutton.width = w;
        pausebutton.height = h;

        var buttontext = game.add.text(pausebutton.x, pausebutton.y, string, {font:"24px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    },
    pause:function(){

          game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'Paused', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(215+game.camera.x,435+game.camera.y,'Resume', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

          /*resumebutton = this.createButton2(game, "Resume", 260, 450, 200, 80, 
          function(){
          });

          exitbutton = this.createButton2(game, "Main Menu", 520, 450, 200, 80, 
          function(){
          });*/

          game.input.onDown.add(this.unpause, this);
          game.paused = true;

    },
    unpause:function(event){
        if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
                game.paused = false;
                pausetext.destroy();
                pausebackground.destroy();
                exitbutton.destroy();
                resumebutton.destroy();
                resumetext.destroy();
                exittext.destroy();
                

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                this.state.start('MainMenu');
            }
          }
        }
    }
    


};