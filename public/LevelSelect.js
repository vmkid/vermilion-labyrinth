Game.LevelSelect = function (game){



};

Game.LevelSelect.prototype = {

    create:function(game){

        menuText = game.add.text(180,60,'Vermilion Labyrinth', {font: '48px Arial', fill:'#fff', align:'center'}); 

        this.createButton(game, "Start Game", 150, 450, 140, 60, 
        function(){
            this.state.start('Level1');
        });

        this.createButton(game, "Continue Game", 400, 450, 140, 60, 
        function(){
            
        });
        
        this.createButton(game, "Back", 400, 530, 140, 60, 
        function(){
            this.state.start('MainMenu');
        });

    },

    update:function(game){

    },

    createButton:function(game, string, x ,y , w,h, callback){

        var button1 = game.add.button(x,y, 'menubutton', callback, this, 2 ,1,0);

        button1.anchor.setTo(0.5, 0.5);

        button1.width = w;
        button1.height = h;

        var buttontext = game.add.text(button1.x, button1.y, string, {font:"14px Arial" , fill:"#fff", Align:"center"});

        buttontext.anchor.setTo(0.5, 0.5); 

    }

};