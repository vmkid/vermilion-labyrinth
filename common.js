var characters = [];
var enemies = [];
var currentlevel;

var selectedunit = "";
var bulletgroup;
var bullet;
var hitDoor = false;
var shooter;
var shotType;
var shooterhp;

var opentil = [];
var closedtil = [];
var moveScore=0;
var openTiles;
var closedTiles;
var pathTiles;

var tween;
var tweenXarr;
var tweenYarr;
var tweenAnglearr;
var tweenPoint;
var speed = 100;
var tweenReady;
var tweenI;
var tweenSprite;
var dashReturn;

var soundDamageArmor;
var soundDamageFlesh;
var soundWalk;
var soundWhoosh;
var soundBGM1;
var shield = [];

var endenabled = true;
var blockedtiles;
var backgroundlayer2;
var selectedmoved = false;
var phasetext = 'Decision Phase';


function pause(){
    game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'Paused', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(215+game.camera.x,435+game.camera.y,'Resume', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

        

          game.input.onDown.add(unpause, this);
          game.paused = true;
}

function unpause(event){
    if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
                game.paused = false;
                pausetext.destroy();
                pausebackground.destroy();
                exitbutton.destroy();
                resumebutton.destroy();
                resumetext.destroy();
                exittext.destroy();
                

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                game.state.start('MainMenu');
            }
          }
        }
}

function initalizeLevel(level, game){
	gameObj = game;
    if(level == 1){
		currentlevel = 1;
		soundBGM1.play();
        characters = [];
        enemies = [];
        characters.push({ name:'Andrew', health:120, id:'tank1', armor:180, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'tank', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        characters.push({ name:'Bryan', health:180, id: 'damage1', armor:70, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'dps', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        characters.push({ name:'Zach', health:130, id:'support1', armor:130, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'support', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy1', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy1', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy2', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy1', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        
    }   
    else if(level == 2){
		currentlevel = 2
		enemies = [];
		enemies.push({ name:'enemy1', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy2', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy2', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy2', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false}); 
		enemies.push({ name:'enemy3', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy2', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
    	enemies.push({ name:'enemy4', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy2', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy5', health:100, armor:100, cooldownPrep:0, cooldownDash:0, speed:5, spriteObject:null, spriteSheet:'enemy2', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        

	}
    else if(level == 3){
		currentlevel = 3
		enemies = [];
		enemies.push({ name:'enemy1', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy2', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false}); 
		enemies.push({ name:'enemy3', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
    	enemies.push({ name:'enemy4', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy5', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy6', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy7', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false}); 
		enemies.push({ name:'enemy8', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
    	enemies.push({ name:'enemy9', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy10', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
		enemies.push({ name:'enemy11', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
        enemies.push({ name:'enemy12', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false}); 
		enemies.push({ name:'enemy13', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});
    	enemies.push({ name:'enemy14', health:100, armor:50, cooldownPrep:0, cooldownDash:0, speed:2, spriteObject:null, spriteSheet:'turret', action:0, finished:false, targetX:-1, targetY:-1, shotType:'hp', isDead:false});

	}
}

function addShield(x, y, position){
	shield[position] = game.add.graphics();
    shield[position].lineStyle(2, 0x9900FF, 1);
    shield[position].drawRect(x, y, 32, 32);
}

function removeShield(position){
	shield[position].destroy();
}

function shoot(targetx, targety, shooterSprite){

	//set character that shot
	var direction = fartherdirection(shooterSprite, targetx, targety);
	game.time.events.add(Phaser.Timer.SECOND * .5, function(){
                                    shooterSprite.animations.play('idle');
                            }, this);
	shooterSprite.animations.play(direction);
	shooter = shooterSprite;
	hitDoor = false;
    //these are to get the center of the tiles
    var x = Math.round(shooter.body.x + 16);
    var y = Math.round(shooter.body.y + 16);
    targetx = Math.round(targetx*32 + 16);
    targety = Math.round(targety*32 + 16);

    var initPoint = new Phaser.Point(x,y);
    var endPoint = new Phaser.Point(targetx,targety);

    bullet = game.add.sprite(x, y, 'bullet');
    game.physics.arcade.enable(bullet);
    bulletgroup = game.add.group();
    bulletgroup.add(bullet);
    game.world.bringToTop(bulletgroup);


    //velocity calculation
    //dist = a = (a^2 + b^2)^1/2
    var dist = Math.sqrt(Math.pow(Math.abs(x - targetx) + Math.abs(y - targety),2));
    var time = dist/500;

    var xVelocity = (-1)*(x - targetx)/time;
    var yVelocity = (-1)*(y - targety)/time;

    bullet.body.velocity.x = xVelocity;
    bullet.body.velocity.y = yVelocity;

    
}

function bulletCollision(bulletSprite, characterSprite){
    //bulletSprite.kill();
    for(var i=0; i<characters.length; i++){
        if(characters[i].spriteObject == characterSprite && shooter != characters[i].spriteObject){
            dealDamage("character",i);
			//bullet.kill();
			bulletSprite.kill();
        }
    }
    for(var i=0; i<enemies.length; i++){
        if(enemies[i].spriteObject == characterSprite && shooter != enemies[i].spriteObject){
            dealDamage("enemy", i);
			//bullet.kill();
			bulletSprite.kill();
        }
    }
}

function bulletMiss(bulletSprite, blocked){
    bulletSprite.kill();
	act(3);
}

function dealDamage(unit, charnum){
	soundDamageFlesh.play();
	var armor;
	var damage;
	if(unit == "character"){
		//will calculate damage later
		if(shotType == 'armor'){
			characters[charnum].armor -=10;
		}
		else{
			armor = characters[charnum].armor;
			damage = shooterhp - armor;
			if(damage<10){
				damage = 10;
			}
			characters[charnum].health -=damage;
		}
	}
	else{
		if(shotType == 'armor'){
			enemies[charnum].armor -=25;
		}
		else{
			armor = enemies[charnum].armor;
			damage = shooterhp - armor;
			if(damage<10){
				damage = 10;
			}
			enemies[charnum].health -=damage;
		}

	}
	act(3);
}

function act(action){
    if(action==5){
        //Do end of move stuff if we reach this point.
		endturn.input.enabled = true;
        blastbutton.input.enabled=true;
        dashbutton.input.enabled=true;
        //prepbutton.input.enabled=true;
		//kill dead characters
		//checkDeaths();
		movedead();
		//reset everyone's finished value
		var charEnem = characters.concat(enemies);
		for(var i=0; i<charEnem.length; i++){
			charEnem[i].finished=false;
		}
		endenabled = true;
		phasetext.setText('Decision Phase');
        return;
    }
    var actor;
    //Start with a combined array of characters and enemies
    var charEnem = characters.concat(enemies);
    //Iterate until we find a character that's performing this move.
    for(var i=0; i<charEnem.length; i++){
        if(charEnem[i].action==action && !charEnem[i].finished && !charEnem[i].isDead){
            actor=charEnem[i];
        }
    }

    if(actor==null){
        act(action+1);
    }
	else{
		actor.finished=true;
		if (action == 1) {
			//Perform 1st action with actor
			//Temp code
			act(1);
		}
		else if (action == 2 || action==4) {
			if(actor.name == selectedunit){
				selectedmoved = true;
			}
			direction = fartherdirection2(actor.spriteObject, actor.targetX, actor.targetY);
			actor.spriteObject.animations.play(direction);
			game.time.events.add(Phaser.Timer.SECOND * 1, function(){
                                    actor.spriteObject.animations.play('idle');
                            }, this);

			dashReturn = action;
			pathfinding(actor.targetX, actor.targetY, actor.spriteObject);
		}
		else if (action == 3) {
			shotType = actor.shotType;
			shooterhp = actor.health;
			shoot(actor.targetX, actor.targetY, actor.spriteObject);
		}
	}
}

function pathfinding(inputX, inputY, charSprite){
    //if(map.getTile(inputX, inputY, layer).properties.solid==false){
				// openTiles.removeAll(true);
				// closedTiles.removeAll(true);
				// pathTiles.removeAll(true);
				console.log("Moving a sprite to "+inputX+","+inputY);
				opentil = [];
				closedtil = [];
				pathtil=[];
				opentil[0] = { x: Math.round(charSprite.body.x / 32), y: Math.round(charSprite.body.y / 32), g: 0, h: 0, parent: null };
				opentil[0].h = Math.abs(10 * (Math.round((opentil[0].x - inputX) + (opentil[0].y - inputY))));
				var foundPath = false;
				var currentPathSquare;
				do {
					//Find lowest f on the open list
					opentil.sort(function (a, b) { return (b.g + b.h) - (a.g + a.h) });
					//Move it to the closed list
					var currentSquare = opentil.pop();
					closedtil.push(currentSquare);
					//Look at the 8 squares around the current one
					for (var x = currentSquare.x - 1; x <= currentSquare.x + 1; x++) {
						for (var y = currentSquare.y - 1; y <= currentSquare.y + 1; y++) {
							//Ignore if it's blocked, or on the Closed List.
							var onClosed = false;
							if(map.getTile(x, y,blockedtiles)){
								onClosed=true;
							}
							else{
							for (var i = 0; i < closedtil.length; i++) {
								if (closedtil[i].x == x && closedtil[i].y == y) {
									onClosed = true;
								}
							}
							}
							if (!onClosed) {
								var onOpen = false;
								var sq;
								for (var i = 0; i < opentil.length; i++) {
									if (opentil[i].x == x && opentil[i].y == y) {
										onOpen = true;
										sq = opentil[i];
									}
								}
								//If it's on the open list...
								if (onOpen) {
									//Recalculate its G and F based on current square
									var addAmt = 10;
									if (sq.x != currentSquare.x && sq.y != currentSquare.y) {
										addAmt = 14;
									}
									sq.parent = currentSquare;
									sq.g = currentSquare.g + addAmt;
								}
								//If not...
								else {
									//Add it to the Open list. Calculate F, G, and H based on current square
									var newSq = { x: x, y: y, g: 0, h: 0, parent: currentSquare };
									//newSq.h = Math.abs(10*(Math.round((newSq.x - inputX)+(newSq.y - inputY))));
									newSq.h = 10 * Math.round(Math.abs(newSq.x - inputX) + Math.abs(newSq.y - inputY));
									if (x != currentSquare.x && y != currentSquare.y) {
										newSq.g = currentSquare.g + 10;
									}
									else {
										newSq.g = currentSquare.g + 14;
									}
									opentil.push(newSq);
								}
							}
						}
					}
					//Check if the target square is on the closed list, and set foundPath if we have.
					if (currentSquare.x == inputX && currentSquare.y == inputY) {
						foundPath = true;
						currentPathSquare = currentSquare;
					}
				} while (!foundPath);
				while (currentPathSquare.parent !== null) {
					//pathTiles.create(currentPathSquare.x * 32, currentPathSquare.y * 32, 'path');
					currentPathSquare.x=currentPathSquare.x*32;
					currentPathSquare.y=currentPathSquare.y*32;
					pathtil.push(currentPathSquare);
					currentPathSquare = currentPathSquare.parent;
				}
				tweenI = pathtil.length;
				tweenSprite = charSprite;
				tweenReady = true;
				if(dashReturn==4){
					soundWalk.play();
				}
				else{
					soundWhoosh.play();
				}
				console.log("Pathfinding complete. Moving sprite");
				// tweenSprite = charSprite;
				// tween = charSprite.add.tween(charSprite);
				// tweenXarr = [];
				// tweenYarr = [];
				// tweenAnglearr=[];
				// tween.onComplete.add(tweenComplete, this);
				// while (tweenI--) {
				// 	var tile = pathtil[tweenI];
				// 	var ang = Math.atan2(tile.y-tweenYarr[tweenYarr.length-1], tile.x-tweenXarr[tweenXarr.length-1])*180/Math.PI;
				// 	tweenAnglearr.push(ang);
				// 	tweenXarr.push(tile.x);
				// 	tweenYarr.push(tile.y);
				// }
				// tweenAnglearr.splice(0, 1);
				// tween.to({ x: tweenXarr, y: tweenYarr}, speed * tweenXarr.length);
				// if (tween !== null) {
				// 	tween.start();
				// }
				// tweenPoint=0;
				// soundWalk.play();
				//charSprite.animations.play('walk');

			//}
}

function tweenComplete(){
	if(dashReturn==4){
		soundWalk.stop();
	}
	act(dashReturn);
}

function initSounds(){
	soundDamageArmor = game.add.audio('damagearmor');
	soundDamageFlesh = game.add.audio('damageflesh');
	soundWalk = game.add.audio('walk');
	soundWalk.loop=true;
	soundWhoosh = game.add.audio('whoosh');
	soundBGM1 = game.add.audio('bgm1');
	soundBGM1.loop=true;

	//While we're here we'll also initialize the pathfinding groups. Because why the fuck not?
	openTiles=game.add.group();
	closedTiles=game.add.group();
	pathTiles=game.add.group();    
}

// calculation used for finding valid move locations
function calculateDistance(character){
	var x = Math.abs(character.targetX - Math.round(character.spriteObject.x/32));
	var y = Math.abs(character.targetY - Math.round(character.spriteObject.y/32));
	return (x+y);
}

// selects actions for enemies
function ai(level){
	if(level == 1){
		for(var i = 0; i<enemies.length; i++){
			if(enemies[i].isDead == false){
				var num = Math.random()*100
				//values will be added/changed when moving works
				if(num <=30){
					enemies[i].action = 4;
					var validmove = false;
					
					do{
						//move location is +- 0-5 in both direction
						var x;
						var y;
						enemies[i].targetX = Math.round(enemies[i].spriteObject.x/32) + (Math.random()*10)-5;
						enemies[i].targetY = Math.round(enemies[i].spriteObject.y/32) + (Math.random()*10)-5;
						enemies[i].targetX = Math.round(enemies[i].targetX);
						enemies[i].targetY = Math.round(enemies[i].targetY);
						x = enemies[i].targetX;
						y = enemies[i].targetY;
						var distance = calculateDistance(enemies[i]);
						if(distance <= enemies[i].speed){
							var blockedtile = map.getTile(enemies[i].targetX, enemies[i].targetY,blockedtiles);
							var backgroundtile = map.getTileWorldXY(enemies[i].targetX*32, enemies[i].targetX*32, 32,32,backgroundlayer2, false);
							if(blockedtile == null && backgroundtile != null){
								validmove = true;
							}
						}
						if(enemies[i].targetX >= 19  || enemies[i].targetX <=0){
							validmove = false;
						}
						if(enemies[i].targetY >= 24  || enemies[i].targetY <=0){
							validmove = false;
						}
					}while(validmove == false);
				}
				else if(num>30 && num <55){
					enemies[i].action = 3;
					enemies[i].shotType = 'armor';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
					}while(targetDead == true);
				}
				else{
					enemies[i].action = 3;
					enemies[i].shotType = 'hp';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
						for(var j=0; j<enemies.length;j++){
							if(j !=i && enemies[i].targetX == enemies[j].targetX && enemies[i].targetY == enemies[j].targetY){
								validmove = false;
							}
						}
					}while(targetDead == true);
				}
			}
		}
	}
	else if(level == 2){
		for(var i = 0; i<enemies.length; i++){
			if(enemies[i].isDead == false){
				var num = Math.random()*100
				//values will be added/changed when moving works
				if(num <30){
					enemies[i].action = 4;
					var validmove = false;
					
					do{
						//move location is +- 0-5 in both direction
						var x;
						var y;
						enemies[i].targetX = Math.round(enemies[i].spriteObject.x/32) + (Math.random()*10)-5;
						enemies[i].targetY = Math.round(enemies[i].spriteObject.y/32) + (Math.random()*10)-5;
						enemies[i].targetX = Math.round(enemies[i].targetX);
						enemies[i].targetY = Math.round(enemies[i].targetY);
						x = enemies[i].targetX;
						y = enemies[i].targetY;
						var distance = calculateDistance(enemies[i]);
						if(distance <= enemies[i].speed){
							var blockedtile = map.getTile(enemies[i].targetX, enemies[i].targetY,blockedtiles);
							var backgroundtile = map.getTileWorldXY(enemies[i].targetX*32, enemies[i].targetX*32, 32,32,backgroundlayer2, false);
							if(blockedtile == null && backgroundtile != null){
								validmove = true;
							}
						}
						if(enemies[i].targetX >= 34  || enemies[i].targetX <=0){
							validmove = false;
						}
						if(enemies[i].targetY >= 49  || enemies[i].targetY <=0){
							validmove = false;
						}
						for(var j=0; j<enemies.length;j++){
							if(j !=i && enemies[i].targetX == enemies[j].targetX && enemies[i].targetY == enemies[j].targetY){
								validmove = false;
							}
						}
					}while(validmove == false);
				}
				else if(num>=30 && num <75){
					enemies[i].action = 3;
					enemies[i].shotType = 'armor';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
					}while(targetDead == true);
				}
				else{
					enemies[i].action = 3;
					enemies[i].shotType = 'hp';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
					}while(targetDead == true);
				}
			}
		}
	}
	else if(level == 3){
		for(var i = 0; i<enemies.length; i++){
			if(enemies.isDead == false){
				var num = Math.random()*100
				//values will be added/changed when moving works
				if(num >150){
					enemies[i].action = 4;
					var validmove = false;
					
					do{
						//move location is +- 0-5 in both direction
						var x;
						var y;
						enemies[i].targetX = Math.round(enemies[i].spriteObject.x/32) + (Math.random()*4)-2;
						enemies[i].targetY = Math.round(enemies[i].spriteObject.y/32) + (Math.random()*4)-2;
						enemies[i].targetX = Math.round(enemies[i].targetX);
						enemies[i].targetY = Math.round(enemies[i].targetY);
						x = enemies[i].targetX;
						y = enemies[i].targetY;
						var distance = calculateDistance(enemies[i]);
						if(distance <= enemies[i].speed){
							var blockedtile = map.getTile(enemies[i].targetX, enemies[i].targetY,blockedtiles);
							var backgroundtile = map.getTileWorldXY(enemies[i].targetX*32, enemies[i].targetX*32, 32,32,backgroundlayer2, false);
							if(blockedtile == null && backgroundtile != null){
								validmove = true;
							}
						}
						if(enemies[i].targetX >= 39  || enemies[i].targetX <=0){
							validmove = false;
						}
						if(enemies[i].targetY >= 59  || enemies[i].targetY <=0){
							validmove = false;
						}
					}while(validmove == false);
				}
				else if(num>50){
					enemies[i].action = 3;
					enemies[i].shotType = 'armor';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
					}while(targetDead == true);
				}
				else{
					enemies[i].action = 3;
					enemies[i].shotType = 'hp';
					var targetDead = true;
					do{
						var target = Math.floor(Math.random()*3);
						enemies[i].targetX = Math.floor(characters[target].spriteObject.x/32);
						enemies[i].targetY = Math.floor(characters[target].spriteObject.y/32);
						if(characters[target].isDead == false){
							targetDead = false;
						}
					}while(targetDead == true);
				}
			}
		}
	}
}

//sets units status to dead when health <0
function checkDeaths(){
	for(var i = 0; i<characters.length; i++){
		if(characters[i].health<=0){
			characters[i].isDead = true;
			//characters[i].animations.play('die');
			//characters[i].spriteObject.kill();
		}
	}
	for(var i = 0; i<enemies.length; i++){
		if(enemies[i].health<=0){
			enemies[i].isDead = true;
			//enemies[i].spriteObject.kill();
		}
	}
}

//win pop up menu
function winLevel(){
    game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(270+game.camera.x,70+game.camera.y,'Level Complete', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(210+game.camera.x,435+game.camera.y,'Next Level', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

        

          game.input.onDown.add(nextLevel, this);
          game.paused = true;
}

//loss pop up menu
function loseLevel(){
    game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'Defeat', {font: '40px Arial', fill:'#fff', align:'center'}); 

          resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(190+game.camera.x,435+game.camera.y,'Restart game', {font: '24px Arial', fill:'#fff', align:'center'}); 

          exitbutton = pausegroup.create(420+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(465+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

        

          game.input.onDown.add(restartGame, this);
          game.paused = true;
}

//will be final victory screen for level 3
function winGame(){
    game.world.bringToTop(pausegroup);
          pausebackground = pausegroup.create(75+game.camera.x, 50+game.camera.y, 'pausemenu');
          pausetext = game.add.text(340+game.camera.x,70+game.camera.y,'You Win!', {font: '40px Arial', fill:'#fff', align:'center'}); 

          /*resumebutton = pausegroup.create(160+game.camera.x, 410+game.camera.y, 'pausedbutton');
          resumebutton.width = 200;
          resumebutton.height = 80;
          resumetext = game.add.text(210+game.camera.x,435+game.camera.y,'Next Level', {font: '24px Arial', fill:'#fff', align:'center'});*/ 

          exitbutton = pausegroup.create(310+game.camera.x, 410+game.camera.y, 'pausedbutton');
          exitbutton.width = 200;
          exitbutton.height = 80;
          exittext = game.add.text(345+game.camera.x,435+game.camera.y,'Main Menu', {font: '24px Arial', fill:'#fff', align:'center'}); 

        

          game.input.onDown.add(win, this);
          game.paused = true;
}

//input listener for victory screen
function nextLevel(event){
    if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
                if(currentlevel==1){
					game.paused = false;
					game.state.start('Level2');
				}
				else if(currentlevel==2){
					game.paused = false;
					game.state.start('Level3');
				}
                

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                game.state.start('MainMenu');
            }
          }
        }
}

//input listener for loss screen
function restartGame(event){
    if(game.paused){
          if(event.y>410 && event.y <490){
            if(event.x > 160 && event.x < 360){
				

            }
            else if(event.x > 420 && event.x <620){
                game.paused = false;
                game.state.start('MainMenu');
            }
          }
        }
}

//if stats go negative sets them to 0 for better visuals
function fixStats(){
	for(var i=0; i<characters.length; i++){
		if(characters[i].health<0){
			characters[i].health = 0;
		}
		if(characters[i].armor<0){
			characters[i].armor = 0;
		}
	}
	for(var i=0; i<enemies.length; i++){
		if(enemies[i].health<0){
			enemies[i].health = 0;
		}
		if(enemies[i].armor<0){
			enemies[i].armor = 0;
		}
	}
}

//cheat that lets character be invincible
function invincible(){
	characters[0].health = 1000000;
	characters[1].health = 1000000;
	characters[2].health = 1000000;
}

function setBlocked(blocked){
	blockedtiles = blocked;
}
function setBackground(background){
	backgroundlayer2 = background;
}

function checkLocations(){
	var same = false;
	for(var i=0; i<characters.length; i++){
		for(var j=i+1; j<characters.length; j++){
			if(characters[i].targetX == Math.round(characters[j].spriteObject.x/32) && characters[i].targetY == Math.round(characters[j].spriteObject.y/32) && characters[j].action!=3){
				same = true;
			}
		}
	}
	for(var i=0; i<characters.length; i++){
		for(var j=i+1; j<characters.length; j++){
			if(characters[i].targetX == characters[j].targetX && characters[i].targetY == characters[j].targetY && characters[j].action!=3){
				same = true;
			}
		}
	}

	return same;

}

function win(event){
    if(game.paused){
		if(event.y>410 && event.y <490){
          	if(event.x > 310 && event.x <510){
                game.paused = false;
                game.state.start('MainMenu');
            }
		}
	}
}

function movedead(){
	for(var i=0; i<characters.length;i++){
		if(characters[i].health <=0){
			//characters[i].spriteObject.x = -100;
			//characters[i].spriteObject.y = -100;
			killunit(characters[i].spriteObject);
		}
	}
	for(var i=0; i<enemies.length;i++){
		if(enemies[i].health <=0){
			killunit(enemies[i].spriteObject);
		}
	}
}

function fartherdirection(sprite, x,y){
    var xdist = sprite.x-x*32;
    var ydist = sprite.y-y*32; 
    if(Math.abs(xdist)>Math.abs(ydist)){
        if(xdist<0){
         return 'right';
        }
        else{
            return 'left';
        }
    }
    else{
        if(ydist<0){
         return 'down';
        }
        else{
            return 'up';
        }
    }
}

function fartherdirection2(sprite, x,y){
    var xdist = sprite.x-x*32;
    var ydist = sprite.y-y*32; 
    if(Math.abs(xdist)>Math.abs(ydist)){
        if(xdist<0){
         return 'm-right';
        }
        else{
            return 'm-left';
        }
    }
    else{
        if(ydist<0){
         return 'm-down';
        }
        else{
            return 'm-up';
        }
    }
}

function killunit(sprite){
	sprite.animations.play('die');
	game.time.events.add(Phaser.Timer.SECOND * .5, function(){
                                    sprite.x = -100;
									sprite.y = -100;
									sprite.kill()
                            }, this);
}